import Vue from 'vue'
import Router from 'vue-router'
import HotkeyPage from './views/HotkeyPage.vue'
import idea from './data/idea.json'
import rider from './data/rider.json'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/idea'
    },
    {
      path: '/idea',
      component: HotkeyPage,
      props: {
        source: idea,
        name: 'idea'
      }
    },
    {
      path: '/rider',
      component: HotkeyPage,
      props: {
        source: rider,
        name: 'rider'
      }
    }
  ]
})
