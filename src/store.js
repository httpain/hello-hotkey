import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    appHotkeys: {},
    progress: {},
    activeApp: '',
    showInfoOnStartup: true
  },
  mutations: {
    setActiveApp(state, app) {
      state.activeApp = app;
    },
    loadHotkeys(state, hotkeys) {
      state.appHotkeys[state.activeApp] = hotkeys;
    },
    incrementProgress(state, itemKey) {
      const fullKey = `${state.activeApp} :: ${itemKey}`;
      if (!state.progress.hasOwnProperty(fullKey)) {
        Vue.set(state.progress, fullKey, 1); // set to 0 and immediately increment
      } else if (state.progress[fullKey] === 2) {
        state.progress[fullKey] = 0 // reset back to first stage
      } else {
        state.progress[fullKey]++
      }
    },
    disableInfoOnStartup(state) {
      state.showInfoOnStartup = false;
    }
  },
  actions: {},
  getters: {
    getProgress(state) {
      return itemKey => state.progress[`${state.activeApp} :: ${itemKey}`]
    }
  },
  plugins: [createPersistedState({
    key: 'hello-hotkey',
    paths: ['appHotkeys', 'progress', 'showInfoOnStartup']
  })]
})
